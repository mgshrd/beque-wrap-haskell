type BequeResult ok err = Prelude.Either err ok

coqstring2string :: String.Coq_string -> Prelude.String
coqstring2string s = case s of { String.EmptyString -> ""; String.String c sx -> c:(coqstring2string sx) }
string2coqstring :: Prelude.String -> String.Coq_string
string2coqstring s = Prelude.foldr (\e accu -> if Data.Char.ord e Prelude.> 255 then Prelude.error ("invalid coq character " Prelude.++ (Prelude.show Prelude.$ Data.Char.ord e)) else String.String e accu) String.EmptyString s

instance Prelude.Show String.Coq_string where show = coqstring2string
instance Prelude.Read String.Coq_string where readsPrec = \_ s -> [(string2coqstring s, "")]
