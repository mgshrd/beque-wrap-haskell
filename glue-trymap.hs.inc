trymap :: Prelude.IO a -> Prelude.IO (Prelude.Either String.Coq_string a)
trymap = ((Prelude.fmap (Data.Bifunctor.first Prelude.$ string2coqstring Prelude.. Prelude.show)) :: (Prelude.Functor f, Data.Bifunctor.Bifunctor g) => f (g Control.Exception.SomeException b) -> f (g String.Coq_string b)) Prelude.. Control.Exception.try
