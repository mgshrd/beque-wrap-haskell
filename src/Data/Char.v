Require Import BinInt.

Axiom Char : Set.
Extract Constant Char => "Data.Char.Char".
Axiom eq_Char_dec : forall c1 c2 : Char, {c1=c2}+{c1<>c2}.
Extract Constant eq_Char_dec => "(Prelude.==)".

Axiom ord : Char -> {codepoint : Z | codepoint <= 1114111}%Z.
Extract Constant ord => "ord".
Axiom chr : {codepoint : Z | codepoint <= 1114111}%Z -> Char.
Extract Constant chr => "chr".
