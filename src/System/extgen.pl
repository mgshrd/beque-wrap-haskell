my %specials = (
#Axiom fixIO : (a -> IO a) -> IO a.
#fixIO :: (a -> IO a) -> IO a
#	"fixIO" => "Extract Constant fixIO => \"trymap Prelude.. System.IO.fixIO\".",
#Axiom interact : (string -> string) -> IO unit.
#interact :: (String -> String) -> IO ()
	"interact" => "Extract Constant interact => \"\\f -> trymap Prelude.\$ System.IO.interact (Prelude.show Prelude.. f Prelude.. Prelude.read)\".",
#Axiom withFile : forall {R}, FilePath -> IOMode -> (Handle -> IO R) -> IO R.
#withFile :: FilePath -> IOMode -> (Handle -> IO r) -> IO r
	"withFile" => "Extract Constant withFile => \"\\fp m hf -> System.IO.withFile (Prelude.show fp) m hf\".",
#Axiom readIO : forall {A} (read : string -> result A string), string -> IO A.
#readIO :: Read a => String -> IO a
	#"readIO" => "\\r s -> Prelude.return (r s)",
#Axiom readLn : forall {A} (read : string -> result A string), IO A.
#readLn :: Read a => IO a
	#"readLn" => "\\r -> Prelude.fmap (r Prelude.. read) System.IO.readLn",
#Axiom withBinaryFile : forall {A}, FilePath -> IOMode -> (Handle -> IO A) -> IO A.
#withBinaryFile :: FilePath -> IOMode -> (Handle -> IO r) -> IO r
	"withBinaryFile" => "Extract Constant withBinaryFile => \"\\fp m hf -> System.IO.withBinaryFile (Prelude.show fp) m hf\".",
);
my %idmap = (
	"IOMode" => 1,
	"Handle" => 1,
	"BufferMode" => 1,
	"HandlePosn" => 1,
	"bool" => 1,
	"Ptr a" => 1,
	"Ptr A" => 1,
	"TextEncoding" => 1,
	"NewlineMode" => 1,
	"SeekMode" => 1,
	"Z" => 1,
	"Char" => 1,
);
my %haskell2coq = (
	"string" => "Prelude.read",
	"FilePath" => "Prelude.read",
	"Z32" => "Prelude.toInteger",
);
sub h2c {
	my $ty = shift;
	if ($ty =~ /\(prodS (.*) (.*)\)/) {
		return "(Data.Bifunctor.bimap (\\f -> ".h2c($1)." f) (\\s -> ".h2c($2)." s))";
	} elsif (exists $haskell2coq{$ty}) {
		return $haskell2coq{$ty};
	} else {
		return "";
	}
}
my %coq2haskell = (
	"string" => "Prelude.show",
	"FilePath" => "Prelude.show",
	"Z32" => "Prelude.fromInteger",
);
sub c2h {
	my $ty = shift;
	my $n = shift;
	my $dbg = shift;
	my $res = "";
	if ($ty =~ /\(prodS (.*) (.*)\)/) {
		$res = "(".c2h($1, "(fst $n)", $dbg).", ".c2h($2, "(snd $n)", $dbg).")";
	} elsif ($ty =~ /^[a-zA-Z]$/) {
		$res = $n;
	} elsif (exists $coq2haskell{$ty}) {
		$res = "($coq2haskell{$ty} $n)";
	} elsif (exists $idmap{$ty}) {
		$res = $n;
	} else {
		die "Unkown type: \"$ty\" in $dbg";
	}
	return $res;
}
while (<>) {
	print;
	s/\(\*[^*]*\*\)//;
	s/  *$//;
	my $lres = "";
	if (/^Axiom [^ ]+_segment : /) {
		next; # not computationally relevant
	} elsif (/^Axiom ([^ ]+) : ([A-Za-z]+)\.$/) {
		my $tmp = h2c $2;
		$tmp = "$tmp " if $tmp;
		$lres = "Extract Constant $1 => \"${tmp}System.IO.$1\"."
	} elsif (/^Axiom (eq_[^ ]+_dec) : /) {
		$lres = "Extract Constant $1 => \"(Prelude.==)\"."
	} elsif  (/^Axiom ([^ ]+) : (.*)IO ([^>]+) \([a-zA-Z0-9]+_segment[a0-9 ]*\)\.$/) {
		if  (exists $specials{$1}) {
			$lres = $specials{$1};
		} else {
			my $aname = $1;
			my $resconv = h2c $3;
			my $tmp = $2;
			$tmp =~ s/^forall \{[a-zA-Z] : Set\}, //;
			$tmp =~ s/^forall [^:]+: //;
			$tmp =~ s/, $//;
			my @argtypes = split /, forall [^:]+: /, $tmp;
	
			my $oos = join(" % ", @argtypes);
			my $pn = 0;
			my $params = join(" ", map {my $tmp = $_; $tmp =~ s/.*/a$pn/; $pn++; $tmp} @argtypes);
			$pn = 0;
			my $args= join(" ", map {my $an = $_; $an =~ s/.*/a$pn/; my $tmp = c2h($_, $an, $aname); $pn++; $tmp} @argtypes);
			$resconv = "(Prelude.fmap (Data.Bifunctor.second $resconv)) Prelude.\$ " if $resconv;
			$exts = "";
			if (0 == scalar @argtypes) {
				$lres = "Extract Constant $aname => \"${resconv}trymap System.IO.$aname\"."
			#conversion missing if argument implicit } elsif (1 == scalar @argtypes) {
			#	$lres = "Extract Constant $aname => \"${resconv}trymap Prelude.. System.IO.$aname\"."
			} else {
				$lres = "Extract Constant $aname => \"\\$params -> ${resconv}trymap Prelude.\$ System.IO.$aname $args\"."
			}
		}
	 }
	if (/^Axiom/ && $lres eq "") {
		print "Skipping $_";
	} elsif ($lres) {
		print $lres."\n";
	}
}
