Require Import String.
Require Import BinInt.
Require Import Beque.Util.result.

Require Import Beque.IO.intf.World.
Require Import Beque.IO.intf.IORaw.

Require Import Haskell.Foreign.Ptr.
Require Import Haskell.Data.Char.

Axiom Handle : Set.
Extract Constant Handle => "System.IO.Handle".
Axiom eq_Handle_dec : forall h1 h2 : Handle, {h1=h2}+{h1<>h2}.
Extract Constant eq_Handle_dec => "(Prelude.==)".

Axiom HandlePosn : Set.
Extract Constant HandlePosn => "System.IO.HandlePosn".
Axiom eq_HandlePosn_dec : forall p1 p2 : HandlePosn, {p1=p2}+{p1<>p2}.
Extract Constant eq_HandlePosn_dec => "(Prelude.==)".

Definition FilePath := string.

Inductive IOMode :=
| ReadMode
| WriteMode
| AppendMode
| ReadWriteMode.
Extract Inductive IOMode => "System.IO.IOMode" ["System.IO.ReadMode" "System.IO.WriteMode" "System.IO.AppendMode" "System.IO.ReadWriteMode"].

Definition bZ := Zpos (xO (xO (xO (xO (xO (xO (xO (xO xH)))))))).
Definition wZ := (bZ*bZ*bZ*bZ)%Z.
Definition Z32 := {z | 0 <= z < wZ}%Z.

Inductive BufferMode :=
| NoBuffering
| LineBuffering
| BlockBuffering : option Z32 -> BufferMode.
Extract Inductive BufferMode => "System.IO.BufferMode" ["System.IO.NoBuffering" "System.IO.LineBuffering" "System.IO.BlockBuffering Prelude.. (Prelude.fmap Prelude.formInteger)"] "(\fNB fLB fBB bm -> case bm of { System.IO.NoBuffering -> fNB (); System.IO.LineBuffering -> fLB (); System.IO.BlockBuffering bs -> fBB (Prelude.fmap Prelude.toInteger bs) })".

Inductive SeekMode :=
| AbsoluteSeek
| RelativeSeek
| SeekFromEnd.
Extract Inductive SeekMode => "System.IO.SeekMode" ["System.IO.AbsoluteSeek" "System.IO.RelativeSeek" "System.IO.SeekFromEnd"].

Inductive Newline :=
| LF
| CRLF.
Extract Inductive Newline => "System.IO.Newline" ["System.IO.LF" "System.IO.CRLF"].

Inductive NewlineMode :=
| mk_NewlineMode : forall (inputNL outputNL : Newline), NewlineMode.
Extract Inductive NewlineMode => "System.IO.NewlineMode" ["\i o -> NewlineMode { inputNL = i, outputNL = o }"] "(\fmk nlm -> fmk (System.IO.inputNL nlm) (System.IO.outputNL nlm))".

Definition prodS : Set -> Set -> Set := prod.

Axiom TextEncoding : Set.
Extract Constant TextEncoding => "System.IO.TextEncoding".


(* Wrapper for Haskell's System.IO

Documentation applies from the URL below, except exceptions are converted to [result A string].
http://hackage.haskell.org/package/base-4.9.0.0/docs/System-IO.html
*)
Module IO
       (w : World)
       (ior : IORaw w)
.

Definition IO A sega : Set := ior.IO (fun _ => True) (result A string) (fun r s _ => sega r s).
(* fixIO :: (a -> IO a) -> IO a *)
(* skipping (a -> IO a (fun _ _ => True)) -> IO a (fixIO_segment *)
(* stdin :: Handle *)
Axiom stdin : Handle.
Extract Constant stdin => "System.IO.stdin".
(* stdout :: Handle *)
Axiom stdout : Handle.
Extract Constant stdout => "System.IO.stdout".
(* stderr :: Handle *)
Axiom stderr : Handle.
Extract Constant stderr => "System.IO.stderr".
(* withFile :: FilePath -> IOMode -> (Handle -> IO r) -> IO r *)
Axiom withFile_segment : forall {r : Set}, FilePath -> IOMode -> (Handle -> IO r (fun _ _ => True)) -> result r string -> list w.msg -> Prop.
Axiom withFile : forall {r : Set}, forall a0 : FilePath, forall a1 : IOMode, forall a2 : (Handle -> IO r (fun _ _ => True)), IO r (withFile_segment a0 a1 a2).
Extract Constant withFile => "\fp m hf -> System.IO.withFile (Prelude.show fp) m hf".
(* openFile :: FilePath -> IOMode -> IO Handle *)
Axiom openFile_segment : FilePath -> IOMode -> result Handle string -> list w.msg -> Prop.
Axiom openFile : forall a0 : FilePath, forall a1 : IOMode, IO Handle (openFile_segment a0 a1).
Extract Constant openFile => "\a0 a1 -> trymap Prelude.$ System.IO.openFile (Prelude.show a0) a1".
(* hClose :: Handle -> IO () *)
Axiom hClose_segment : Handle -> result unit string -> list w.msg -> Prop.
Axiom hClose : forall a0 : Handle, IO unit (hClose_segment a0).
Extract Constant hClose => "\a0 -> trymap Prelude.$ System.IO.hClose a0".
(* readFile :: FilePath -> IO String *)
Axiom readFile_segment : FilePath -> result string string -> list w.msg -> Prop.
Axiom readFile : forall a0 : FilePath, IO string (readFile_segment a0).
Extract Constant readFile => "\a0 -> (Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap Prelude.$ System.IO.readFile (Prelude.show a0)".
(* writeFile :: FilePath -> String -> IO () *)
Axiom writeFile_segment : FilePath -> string -> result unit string -> list w.msg -> Prop.
Axiom writeFile : forall a0 : FilePath, forall a1 : string, IO unit (writeFile_segment a0 a1).
Extract Constant writeFile => "\a0 a1 -> trymap Prelude.$ System.IO.writeFile (Prelude.show a0) (Prelude.show a1)".
(* appendFile :: FilePath -> String -> IO () *)
Axiom appendFile_segment : FilePath -> string -> result unit string -> list w.msg -> Prop.
Axiom appendFile : forall a0 : FilePath, forall a1 : string, IO unit (appendFile_segment a0 a1).
Extract Constant appendFile => "\a0 a1 -> trymap Prelude.$ System.IO.appendFile (Prelude.show a0) (Prelude.show a1)".
(* hFileSize :: Handle -> IO Integer *)
Axiom hFileSize_segment : Handle -> result Z string -> list w.msg -> Prop.
Axiom hFileSize : forall a0 : Handle, IO Z (hFileSize_segment a0).
Extract Constant hFileSize => "\a0 -> trymap Prelude.$ System.IO.hFileSize a0".
(* hSetFileSize :: Handle -> Integer -> IO () *)
Axiom hSetFileSize_segment : Handle -> Z -> result unit string -> list w.msg -> Prop.
Axiom hSetFileSize : forall a0 : Handle, forall a1 : Z, IO unit (hSetFileSize_segment a0 a1).
Extract Constant hSetFileSize => "\a0 a1 -> trymap Prelude.$ System.IO.hSetFileSize a0 a1".
(* hIsEOF :: Handle -> IO Bool *)
Axiom hIsEOF_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsEOF : forall a0 : Handle, IO bool (hIsEOF_segment a0).
Extract Constant hIsEOF => "\a0 -> trymap Prelude.$ System.IO.hIsEOF a0".
(* isEOF :: IO Bool *)
Axiom isEOF_segment : result bool string -> list w.msg -> Prop.
Axiom isEOF : IO bool (isEOF_segment).
Extract Constant isEOF => "trymap System.IO.isEOF".
(* hSetBuffering :: Handle -> BufferMode -> IO () *)
Axiom hSetBuffering_segment : Handle -> BufferMode -> result unit string -> list w.msg -> Prop.
Axiom hSetBuffering : forall a0 : Handle, forall a1 : BufferMode, IO unit (hSetBuffering_segment a0 a1).
Extract Constant hSetBuffering => "\a0 a1 -> trymap Prelude.$ System.IO.hSetBuffering a0 a1".
(* hGetBuffering :: Handle -> IO BufferMode *)
Axiom hGetBuffering_segment : Handle -> result BufferMode string -> list w.msg -> Prop.
Axiom hGetBuffering : forall a0 : Handle, IO BufferMode (hGetBuffering_segment a0).
Extract Constant hGetBuffering => "\a0 -> trymap Prelude.$ System.IO.hGetBuffering a0".
(* hFlush :: Handle -> IO () *)
Axiom hFlush_segment : Handle -> result unit string -> list w.msg -> Prop.
Axiom hFlush : forall a0 : Handle, IO unit (hFlush_segment a0).
Extract Constant hFlush => "\a0 -> trymap Prelude.$ System.IO.hFlush a0".
(* hGetPosn :: Handle -> IO HandlePosn *)
Axiom hGetPosn_segment : Handle -> result HandlePosn string -> list w.msg -> Prop.
Axiom hGetPosn : forall a0 : Handle, IO HandlePosn (hGetPosn_segment a0).
Extract Constant hGetPosn => "\a0 -> trymap Prelude.$ System.IO.hGetPosn a0".
(* hSetPosn :: HandlePosn -> IO () *)
Axiom hSetPosn_segment : HandlePosn -> result unit string -> list w.msg -> Prop.
Axiom hSetPosn : forall a0 : HandlePosn, IO unit (hSetPosn_segment a0).
Extract Constant hSetPosn => "\a0 -> trymap Prelude.$ System.IO.hSetPosn a0".
(* hSeek :: Handle -> SeekMode -> Integer -> IO () *)
Axiom hSeek_segment : Handle -> SeekMode -> Z -> result unit string -> list w.msg -> Prop.
Axiom hSeek : forall a0 : Handle, forall a1 : SeekMode, forall a2 : Z, IO unit (hSeek_segment a0 a1 a2).
Extract Constant hSeek => "\a0 a1 a2 -> trymap Prelude.$ System.IO.hSeek a0 a1 a2".
(* hTell :: Handle -> IO Integer *)
Axiom hTell_segment : Handle -> result Z string -> list w.msg -> Prop.
Axiom hTell : forall a0 : Handle, IO Z (hTell_segment a0).
Extract Constant hTell => "\a0 -> trymap Prelude.$ System.IO.hTell a0".
(* hIsOpen :: Handle -> IO Bool *)
Axiom hIsOpen_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsOpen : forall a0 : Handle, IO bool (hIsOpen_segment a0).
Extract Constant hIsOpen => "\a0 -> trymap Prelude.$ System.IO.hIsOpen a0".
(* hIsClosed :: Handle -> IO Bool *)
Axiom hIsClosed_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsClosed : forall a0 : Handle, IO bool (hIsClosed_segment a0).
Extract Constant hIsClosed => "\a0 -> trymap Prelude.$ System.IO.hIsClosed a0".
(* hIsReadable :: Handle -> IO Bool *)
Axiom hIsReadable_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsReadable : forall a0 : Handle, IO bool (hIsReadable_segment a0).
Extract Constant hIsReadable => "\a0 -> trymap Prelude.$ System.IO.hIsReadable a0".
(* hIsWritable :: Handle -> IO Bool *)
Axiom hIsWritable_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsWritable : forall a0 : Handle, IO bool (hIsWritable_segment a0).
Extract Constant hIsWritable => "\a0 -> trymap Prelude.$ System.IO.hIsWritable a0".
(* hIsSeekable :: Handle -> IO Bool *)
Axiom hIsSeekable_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsSeekable : forall a0 : Handle, IO bool (hIsSeekable_segment a0).
Extract Constant hIsSeekable => "\a0 -> trymap Prelude.$ System.IO.hIsSeekable a0".
(* hIsTerminalDevice :: Handle -> IO Bool *)
Axiom hIsTerminalDevice_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hIsTerminalDevice : forall a0 : Handle, IO bool (hIsTerminalDevice_segment a0).
Extract Constant hIsTerminalDevice => "\a0 -> trymap Prelude.$ System.IO.hIsTerminalDevice a0".
(* hSetEcho :: Handle -> Bool -> IO () *)
Axiom hSetEcho_segment : Handle -> bool -> result unit string -> list w.msg -> Prop.
Axiom hSetEcho : forall a0 : Handle, forall a1 : bool, IO unit (hSetEcho_segment a0 a1).
Extract Constant hSetEcho => "\a0 a1 -> trymap Prelude.$ System.IO.hSetEcho a0 a1".
(* hGetEcho :: Handle -> IO Bool *)
Axiom hGetEcho_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hGetEcho : forall a0 : Handle, IO bool (hGetEcho_segment a0).
Extract Constant hGetEcho => "\a0 -> trymap Prelude.$ System.IO.hGetEcho a0".
(* hShow :: Handle -> IO String *)
Axiom hShow_segment : Handle -> result string string -> list w.msg -> Prop.
Axiom hShow : forall a0 : Handle, IO string (hShow_segment a0).
Extract Constant hShow => "\a0 -> (Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap Prelude.$ System.IO.hShow a0".
(* hWaitForInput :: Handle -> Int -> IO Bool *)
Axiom hWaitForInput_segment : Handle -> Z32 -> result bool string -> list w.msg -> Prop.
Axiom hWaitForInput : forall a0 : Handle, forall a1 : Z32, IO bool (hWaitForInput_segment a0 a1).
Extract Constant hWaitForInput => "\a0 a1 -> trymap Prelude.$ System.IO.hWaitForInput a0 (Prelude.fromInteger a1)".
(* hReady :: Handle -> IO Bool *)
Axiom hReady_segment : Handle -> result bool string -> list w.msg -> Prop.
Axiom hReady : forall a0 : Handle, IO bool (hReady_segment a0).
Extract Constant hReady => "\a0 -> trymap Prelude.$ System.IO.hReady a0".
(* hGetChar :: Handle -> IO Char *)
Axiom hGetChar_segment : Handle -> result Char string -> list w.msg -> Prop.
Axiom hGetChar : forall a0 : Handle, IO Char (hGetChar_segment a0).
Extract Constant hGetChar => "\a0 -> trymap Prelude.$ System.IO.hGetChar a0".
(* hGetLine :: Handle -> IO String *)
Axiom hGetLine_segment : Handle -> result string string -> list w.msg -> Prop.
Axiom hGetLine : forall a0 : Handle, IO string (hGetLine_segment a0).
Extract Constant hGetLine => "\a0 -> (Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap Prelude.$ System.IO.hGetLine a0".
(* hLookAhead :: Handle -> IO Char *)
Axiom hLookAhead_segment : Handle -> result Char string -> list w.msg -> Prop.
Axiom hLookAhead : forall a0 : Handle, IO Char (hLookAhead_segment a0).
Extract Constant hLookAhead => "\a0 -> trymap Prelude.$ System.IO.hLookAhead a0".
(* hGetContents :: Handle -> IO String *)
Axiom hGetContents_segment : Handle -> result string string -> list w.msg -> Prop.
Axiom hGetContents : forall a0 : Handle, IO string (hGetContents_segment a0).
Extract Constant hGetContents => "\a0 -> (Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap Prelude.$ System.IO.hGetContents a0".
(* hPutChar :: Handle -> Char -> IO () *)
Axiom hPutChar_segment : Handle -> Char -> result unit string -> list w.msg -> Prop.
Axiom hPutChar : forall a0 : Handle, forall a1 : Char, IO unit (hPutChar_segment a0 a1).
Extract Constant hPutChar => "\a0 a1 -> trymap Prelude.$ System.IO.hPutChar a0 a1".
(* hPutStr :: Handle -> String -> IO () *)
Axiom hPutStr_segment : Handle -> string -> result unit string -> list w.msg -> Prop.
Axiom hPutStr : forall a0 : Handle, forall a1 : string, IO unit (hPutStr_segment a0 a1).
Extract Constant hPutStr => "\a0 a1 -> trymap Prelude.$ System.IO.hPutStr a0 (Prelude.show a1)".
(* hPutStrLn :: Handle -> String -> IO () *)
Axiom hPutStrLn_segment : Handle -> string -> result unit string -> list w.msg -> Prop.
Axiom hPutStrLn : forall a0 : Handle, forall a1 : string, IO unit (hPutStrLn_segment a0 a1).
Extract Constant hPutStrLn => "\a0 a1 -> trymap Prelude.$ System.IO.hPutStrLn a0 (Prelude.show a1)".
(* hPrint :: Show a => Handle -> a -> IO () *)
Axiom hPrint_segment : forall {a : Set}, Handle -> a -> result unit string -> list w.msg -> Prop.
Axiom hPrint : forall {a : Set}, forall s : (a -> string), forall a0 : Handle, forall a1 : a, IO unit (hPrint_segment a0 a1).
Extract Constant hPrint => "\s a0 a1 -> trymap Prelude.$ System.IO.hPrint a0 (s a1)".
(* interact :: (String -> String) -> IO () *)
Axiom interact_segment : (string -> string) -> result unit string -> list w.msg -> Prop.
Axiom interact : forall a0 : (string -> string), IO unit (interact_segment a0).
Extract Constant interact => "\f -> trymap Prelude.$ System.IO.interact (Prelude.show Prelude.. f Prelude.. Prelude.read)".
(* putChar :: Char -> IO () *)
Axiom putChar_segment : Char -> result unit string -> list w.msg -> Prop.
Axiom putChar : forall a0 : Char, IO unit (putChar_segment a0).
Extract Constant putChar => "\a0 -> trymap Prelude.$ System.IO.putChar a0".
(* putStr :: String -> IO () *)
Axiom putStr_segment : string -> result unit string -> list w.msg -> Prop.
Axiom putStr : forall a0 : string, IO unit (putStr_segment a0).
Extract Constant putStr => "\a0 -> trymap Prelude.$ System.IO.putStr (Prelude.show a0)".
(* putStrLn :: String -> IO () *)
Axiom putStrLn_segment : string -> result unit string -> list w.msg -> Prop.
Axiom putStrLn : forall a0 : string, IO unit (putStrLn_segment a0).
Extract Constant putStrLn => "\a0 -> trymap Prelude.$ System.IO.putStrLn (Prelude.show a0)".
(* print :: Show a => a -> IO () *)
Axiom print_segment : forall {a : Set}, a -> result unit string -> list w.msg -> Prop.
Axiom print : forall {a : Set}, forall s : (a -> string), forall a0 : a, IO unit (print_segment a0).
Extract Constant print => "\s -> trymap Prelude.. System.IO.print Prelude.. s".
(* getChar :: IO Char *)
Axiom getChar_segment : result Char string -> list w.msg -> Prop.
Axiom getChar : IO Char (getChar_segment).
Extract Constant getChar => "trymap System.IO.getChar".
(* getLine :: IO String *)
Axiom getLine_segment : result string string -> list w.msg -> Prop.
Axiom getLine : IO string (getLine_segment).
Extract Constant getLine => "(Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap System.IO.getLine".
(* getContents :: IO String *)
Axiom getContents_segment : result string string -> list w.msg -> Prop.
Axiom getContents : IO string (getContents_segment).
Extract Constant getContents => "(Prelude.fmap (Data.Bifunctor.second Prelude.read)) Prelude.$ trymap System.IO.getContents".
(* readIO :: Read a => String -> IO a *)
(* skipping string -> IO a (readIO_segment *)
(* readLn :: Read a => IO a *)
(* skipping IO a (readLn_segment *)
(* withBinaryFile :: FilePath -> IOMode -> (Handle -> IO r) -> IO r *)
Axiom withBinaryFile_segment : forall {r : Set}, FilePath -> IOMode -> (Handle -> IO r (fun _ _ => True)) -> result r string -> list w.msg -> Prop.
Axiom withBinaryFile : forall {r : Set}, forall a0 : FilePath, forall a1 : IOMode, forall a2 : (Handle -> IO r (fun _ _ => True)), IO r (withBinaryFile_segment a0 a1 a2).
Extract Constant withBinaryFile => "\fp m hf -> System.IO.withBinaryFile (Prelude.show fp) m hf".
(* openBinaryFile :: FilePath -> IOMode -> IO Handle *)
Axiom openBinaryFile_segment : FilePath -> IOMode -> result Handle string -> list w.msg -> Prop.
Axiom openBinaryFile : forall a0 : FilePath, forall a1 : IOMode, IO Handle (openBinaryFile_segment a0 a1).
Extract Constant openBinaryFile => "\a0 a1 -> trymap Prelude.$ System.IO.openBinaryFile (Prelude.show a0) a1".
(* hSetBinaryMode :: Handle -> Bool -> IO () *)
Axiom hSetBinaryMode_segment : Handle -> bool -> result unit string -> list w.msg -> Prop.
Axiom hSetBinaryMode : forall a0 : Handle, forall a1 : bool, IO unit (hSetBinaryMode_segment a0 a1).
Extract Constant hSetBinaryMode => "\a0 a1 -> trymap Prelude.$ System.IO.hSetBinaryMode a0 a1".
(* hPutBuf :: Handle -> Ptr a -> Int -> IO () *)
Axiom hPutBuf_segment : forall {a : Set}, Handle -> Ptr a -> Z32 -> result unit string -> list w.msg -> Prop.
Axiom hPutBuf : forall {a : Set}, forall a0 : Handle, forall a1 : Ptr a, forall a2 : Z32, IO unit (hPutBuf_segment a0 a1 a2).
Extract Constant hPutBuf => "\a0 a1 a2 -> trymap Prelude.$ System.IO.hPutBuf a0 a1 (Prelude.fromInteger a2)".
(* hGetBuf :: Handle -> Ptr a -> Int -> IO Int *)
Axiom hGetBuf_segment : forall {a : Set}, Handle -> Ptr a -> Z32 -> result Z32 string -> list w.msg -> Prop.
Axiom hGetBuf : forall {a : Set}, forall a0 : Handle, forall a1 : Ptr a, forall a2 : Z32, IO Z32 (hGetBuf_segment a0 a1 a2).
Extract Constant hGetBuf => "\a0 a1 a2 -> (Prelude.fmap (Data.Bifunctor.second Prelude.toInteger)) Prelude.$ trymap Prelude.$ System.IO.hGetBuf a0 a1 (Prelude.fromInteger a2)".
(* hGetBufSome :: Handle -> Ptr a -> Int -> IO Int *)
Axiom hGetBufSome_segment : forall {a : Set}, Handle -> Ptr a -> Z32 -> result Z32 string -> list w.msg -> Prop.
Axiom hGetBufSome : forall {a : Set}, forall a0 : Handle, forall a1 : Ptr a, forall a2 : Z32, IO Z32 (hGetBufSome_segment a0 a1 a2).
Extract Constant hGetBufSome => "\a0 a1 a2 -> (Prelude.fmap (Data.Bifunctor.second Prelude.toInteger)) Prelude.$ trymap Prelude.$ System.IO.hGetBufSome a0 a1 (Prelude.fromInteger a2)".
(* hPutBufNonBlocking :: Handle -> Ptr a -> Int -> IO Int *)
Axiom hPutBufNonBlocking_segment : forall {a : Set}, Handle -> Ptr a -> Z32 -> result Z32 string -> list w.msg -> Prop.
Axiom hPutBufNonBlocking : forall {a : Set}, forall a0 : Handle, forall a1 : Ptr a, forall a2 : Z32, IO Z32 (hPutBufNonBlocking_segment a0 a1 a2).
Extract Constant hPutBufNonBlocking => "\a0 a1 a2 -> (Prelude.fmap (Data.Bifunctor.second Prelude.toInteger)) Prelude.$ trymap Prelude.$ System.IO.hPutBufNonBlocking a0 a1 (Prelude.fromInteger a2)".
(* hGetBufNonBlocking :: Handle -> Ptr a -> Int -> IO Int *)
Axiom hGetBufNonBlocking_segment : forall {a : Set}, Handle -> Ptr a -> Z32 -> result Z32 string -> list w.msg -> Prop.
Axiom hGetBufNonBlocking : forall {a : Set}, forall a0 : Handle, forall a1 : Ptr a, forall a2 : Z32, IO Z32 (hGetBufNonBlocking_segment a0 a1 a2).
Extract Constant hGetBufNonBlocking => "\a0 a1 a2 -> (Prelude.fmap (Data.Bifunctor.second Prelude.toInteger)) Prelude.$ trymap Prelude.$ System.IO.hGetBufNonBlocking a0 a1 (Prelude.fromInteger a2)".
(* openTempFile :: FilePath -> String -> IO (FilePath, Handle) *)
Axiom openTempFile_segment : FilePath -> string -> result (prodS FilePath Handle) string -> list w.msg -> Prop.
Axiom openTempFile : forall a0 : FilePath, forall a1 : string, IO (prodS FilePath Handle) (openTempFile_segment a0 a1).
Extract Constant openTempFile => "\a0 a1 -> (Prelude.fmap (Data.Bifunctor.second (Data.Bifunctor.bimap (\f -> Prelude.read f) (\s ->  s)))) Prelude.$ trymap Prelude.$ System.IO.openTempFile (Prelude.show a0) (Prelude.show a1)".
(* openBinaryTempFile :: FilePath -> String -> IO (FilePath, Handle) *)
Axiom openBinaryTempFile_segment : FilePath -> string -> result (prodS FilePath Handle) string -> list w.msg -> Prop.
Axiom openBinaryTempFile : forall a0 : FilePath, forall a1 : string, IO (prodS FilePath Handle) (openBinaryTempFile_segment a0 a1).
Extract Constant openBinaryTempFile => "\a0 a1 -> (Prelude.fmap (Data.Bifunctor.second (Data.Bifunctor.bimap (\f -> Prelude.read f) (\s ->  s)))) Prelude.$ trymap Prelude.$ System.IO.openBinaryTempFile (Prelude.show a0) (Prelude.show a1)".
(* openTempFileWithDefaultPermissions :: FilePath -> String -> IO (FilePath, Handle) *)
Axiom openTempFileWithDefaultPermissions_segment : FilePath -> string -> result (prodS FilePath Handle) string -> list w.msg -> Prop.
Axiom openTempFileWithDefaultPermissions : forall a0 : FilePath, forall a1 : string, IO (prodS FilePath Handle) (openTempFileWithDefaultPermissions_segment a0 a1).
Extract Constant openTempFileWithDefaultPermissions => "\a0 a1 -> (Prelude.fmap (Data.Bifunctor.second (Data.Bifunctor.bimap (\f -> Prelude.read f) (\s ->  s)))) Prelude.$ trymap Prelude.$ System.IO.openTempFileWithDefaultPermissions (Prelude.show a0) (Prelude.show a1)".
(* openBinaryTempFileWithDefaultPermissions :: FilePath -> String -> IO (FilePath, Handle) *)
Axiom openBinaryTempFileWithDefaultPermissions_segment : FilePath -> string -> result (prodS FilePath Handle) string -> list w.msg -> Prop.
Axiom openBinaryTempFileWithDefaultPermissions : forall a0 : FilePath, forall a1 : string, IO (prodS FilePath Handle) (openBinaryTempFileWithDefaultPermissions_segment a0 a1).
Extract Constant openBinaryTempFileWithDefaultPermissions => "\a0 a1 -> (Prelude.fmap (Data.Bifunctor.second (Data.Bifunctor.bimap (\f -> Prelude.read f) (\s ->  s)))) Prelude.$ trymap Prelude.$ System.IO.openBinaryTempFileWithDefaultPermissions (Prelude.show a0) (Prelude.show a1)".
(* hSetEncoding :: Handle -> TextEncoding -> IO () *)
Axiom hSetEncoding_segment : Handle -> TextEncoding -> result unit string -> list w.msg -> Prop.
Axiom hSetEncoding : forall a0 : Handle, forall a1 : TextEncoding, IO unit (hSetEncoding_segment a0 a1).
Extract Constant hSetEncoding => "\a0 a1 -> trymap Prelude.$ System.IO.hSetEncoding a0 a1".
(* hGetEncoding :: Handle -> IO (Maybe TextEncoding) *)
Axiom hGetEncoding_segment : Handle -> result (option TextEncoding) string -> list w.msg -> Prop.
Axiom hGetEncoding : forall a0 : Handle, IO (option TextEncoding) (hGetEncoding_segment a0).
Extract Constant hGetEncoding => "\a0 -> trymap Prelude.$ System.IO.hGetEncoding a0".
(* latin1 :: TextEncoding *)
Axiom latin1 : TextEncoding.
Extract Constant latin1 => "System.IO.latin1".
(* utf8 :: TextEncoding *)
Axiom utf8 : TextEncoding.
Extract Constant utf8 => "System.IO.utf8".
(* utf8_bom :: TextEncoding *)
Axiom utf8_bom : TextEncoding.
Extract Constant utf8_bom => "System.IO.utf8_bom".
(* utf16 :: TextEncoding *)
Axiom utf16 : TextEncoding.
Extract Constant utf16 => "System.IO.utf16".
(* utf16le :: TextEncoding *)
Axiom utf16le : TextEncoding.
Extract Constant utf16le => "System.IO.utf16le".
(* utf16be :: TextEncoding *)
Axiom utf16be : TextEncoding.
Extract Constant utf16be => "System.IO.utf16be".
(* utf32 :: TextEncoding *)
Axiom utf32 : TextEncoding.
Extract Constant utf32 => "System.IO.utf32".
(* utf32le :: TextEncoding *)
Axiom utf32le : TextEncoding.
Extract Constant utf32le => "System.IO.utf32le".
(* utf32be :: TextEncoding *)
Axiom utf32be : TextEncoding.
Extract Constant utf32be => "System.IO.utf32be".
(* localeEncoding :: TextEncoding *)
Axiom localeEncoding : TextEncoding.
Extract Constant localeEncoding => "System.IO.localeEncoding".
(* char8 :: TextEncoding *)
Axiom char8 : TextEncoding.
Extract Constant char8 => "System.IO.char8".
(* mkTextEncoding :: String -> IO TextEncoding *)
Axiom mkTextEncoding_segment : string -> result TextEncoding string -> list w.msg -> Prop.
Axiom mkTextEncoding : forall a0 : string, IO TextEncoding (mkTextEncoding_segment a0).
Extract Constant mkTextEncoding => "\a0 -> trymap Prelude.$ System.IO.mkTextEncoding (Prelude.show a0)".
(* hSetNewlineMode :: Handle -> NewlineMode -> IO () *)
Axiom hSetNewlineMode_segment : Handle -> NewlineMode -> result unit string -> list w.msg -> Prop.
Axiom hSetNewlineMode : forall a0 : Handle, forall a1 : NewlineMode, IO unit (hSetNewlineMode_segment a0 a1).
Extract Constant hSetNewlineMode => "\a0 a1 -> trymap Prelude.$ System.IO.hSetNewlineMode a0 a1".
(* nativeNewline :: Newline *)
Axiom nativeNewline : Newline.
Extract Constant nativeNewline => "System.IO.nativeNewline".
(* noNewlineTranslation :: NewlineMode *)
Axiom noNewlineTranslation : NewlineMode.
Extract Constant noNewlineTranslation => "System.IO.noNewlineTranslation".
(* universalNewlineMode :: NewlineMode *)
Axiom universalNewlineMode : NewlineMode.
Extract Constant universalNewlineMode => "System.IO.universalNewlineMode".
(* nativeNewlineMode :: NewlineMode *)
Axiom nativeNewlineMode : NewlineMode.
Extract Constant nativeNewlineMode => "System.IO.nativeNewlineMode".
End IO.
