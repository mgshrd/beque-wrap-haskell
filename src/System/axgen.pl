my %skipped = (
	"fixIO" => 1,
	"readIO" => 1,
	"readLn" => 1,
);
my $p = 0;
while (<>) {
	if ($p and /^     \*/ and not / data / and not / type /) {
		chomp;
		s/  */ /g;
		s/ \* //;
		print "(* $_ *)\n";
		s/$/./;
		s/::/:/;
		s/^/Axiom /;
		@tcreqs = ();
		s/: [A-Za-z]* [a-z]* => /: /;
		my %tvarsf = ();
		my @tvarstmp = ();
		while (/\b([a-z])\b/g) {
			unless (exists $tvarsf{$1}) {
				@tvarstmp = (@tvarstmp, $1);
				$tvarsf{$1} = 1;
			}
		}
		my $tvars = join(" ", @tvarstmp);
		while (s/\(([a-zA-Z0-9 ()]+), ([a-zA-Z0-9 ()]+)\)/(prodS \1 \2)/) {
		}
		s/String/string/g;
		s/Integer/Z/g;
		s/Int/Z32/g;
		s/Bool/bool/g;
		s/\(\)/unit/g;
		s/Maybe/option/g;
		die "Missing 'Axiom' in $_" unless s/^Axiom ([^ ]+) : //;
		my $axname = $1;
		my $resty = "";
		if (s/IO ([a-zA-Z0-9 ()]+)\./IO $1 (${axname}_segment/g) {
			$resty = $1; # final action
		}
		s/IO ([a-zA-Z0-9 ()]+)\) ->/IO $1 (fun _ _ => True)) ->/g; # intermediate actions
		if (exists $skipped{$axname}) {
			print "(* skipping $_ *)\n";
		} else {
			my @types = ();
			my $maxl = 100;
			while (/->/) {
				$maxl--;
				while (s/^(\((?:[^()]++|(?1))*\)) -> //g) {
					@types = (@types, $1);
				}
				while (s/^([a-zA-Z0-9 ]*) -> //) {
					@types = (@types, $1);
				}
				die "looping $_" if $maxl < 1;
			}
			my $ax = "Axiom $axname :";
			$ax = "$ax forall {$tvars : Set}," unless $tvars eq "";
			my $an = 0;
			my $sega = "Axiom ${axname}_segment : XXARGSXXresult $resty string -> list w.msg -> Prop.";
			for my $t (@types) {
				$ax = "$ax forall a$an : $t,";
				s/(IO .*)$/$1 a$an/;
				$sega =~ s/XXARGSXX/$t -> XXARGSXX/;
				$an++;
			}
			$sega =~ s/XXARGSXX//;
			if ($resty) {
				#if ($axname eq "withFile" or $axname eq "withBinaryFile") {
				#	$sega =~ s/_segment :/_segment : forall {r : Set},/;
				#}
				unless ($tvars eq "") {
					$sega =~ s/ : / : forall {$tvars : Set}, /;
				}
				print "$sega\n";
				s/$/)./;
			}
			print "$ax $_\n";
		}
	} elsif (/Synopsis/) {
		$p = 2;
	} elsif (/^$/) {
		$p-- if $p;
	}
}
