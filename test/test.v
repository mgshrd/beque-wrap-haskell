Require Import Beque.IO.impl.AxiomaticIO.
Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import System.IO.
Require Import common_extract.

Require Import DelayExtractionDefinitionToken.
Module io.
  Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End io.

Module sio.
  Include IO UnitWorld AxiomaticIOInAUnitWorld.
End sio.

Export sio.
